package org.gcube.service.idm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.keycloak.representations.idm.GroupRepresentation;
import org.slf4j.LoggerFactory;

public class KCGroupsController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(KCGroupsController.class);

	public enum REPR {
		full, compact, name, id, path, none
	}

	public static Object formatRepr(GroupRepresentation group, REPR format) {
		if (group == null || format.equals(REPR.none)) {
			return null;
		}
		if (format.equals(REPR.name)) {
			return group.getName();
		}

		if (group.equals(REPR.id)) {
			return group.getId();
		}

		if (group.equals(REPR.path)) {
			return group.getPath();
		}

		if (format.equals(REPR.compact)) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("id", group.getId());
			result.put("name", group.getName());
			result.put("path", group.getPath());
			return result;
		}

		return group;
	}

	public static List<Object> formatList(List<GroupRepresentation> groups, REPR format) {
		return groups.stream().map(x -> formatRepr(x, format)).filter(Objects::nonNull).collect(Collectors.toList());
	}

}
