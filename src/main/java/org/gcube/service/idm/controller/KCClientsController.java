package org.gcube.service.idm.controller;

import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.gcube.service.idm.keycloack.KkClientFactory;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.NotFoundException;

public class KCClientsController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(KCClientsController.class);

	public enum REPR {
		full, compact, name, id, client_id, none
	}

	public static Object formatRepr(ClientRepresentation client, REPR format) {
		if (client == null || format.equals(REPR.none)) {
			return null;
		}
		if (format.equals(REPR.name)) {
			return client.getName();
		}

		if (format.equals(REPR.id)) {
			return client.getId();
		}

		if (format.equals(REPR.client_id)) {
			return client.getClientId();
		}

		if (format.equals(REPR.compact)) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			result.put("id", client.getId());
			result.put("clientId", client.getClientId());
			result.put("name", client.getName());
			result.put("description", client.getDescription());
			return result;
		}

		return client;
	}

	public static List<Object> formatList(List<ClientRepresentation> clients, REPR format) {
		return clients.stream().map(x -> formatRepr(x, format)).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public static List<RoleRepresentation> getRolesCurrent() {
		logger.info("Searching users for context");
		ClientResource client = KkClientFactory.getSingleton().getKKClient();
		RolesResource roles_resource = client.roles();

		List<RoleRepresentation> roles = roles_resource.list();
		return roles;
	}

	public static RoleRepresentation getRoleByNameCurrent(String name) {
		return getRoleByName(null, name);
	}

	public static RoleRepresentation getRoleByName(String clientId, String name) {

		logger.info("Searching users for context");
		ClientResource clientResource = KkClientFactory.getSingleton().getKKClientById(clientId);
		RolesResource roles_resource = clientResource.roles();
		RoleResource role = roles_resource.get(name);
		if (role == null) {
			throw new NotFoundException("cannot retrieve role " + name);
		}

		return role.toRepresentation();
	}

	public static List<UserRepresentation> getContextUsersByRoleCurrent(String role_name) {
		return getContextUsersByRoleCurrent(role_name, null, null);

	}

	public static List<UserRepresentation> getContextUsersByRoleCurrent(String role_name, Integer firstResult,
			Integer maxResults) {
		return getContextUsersByRole(null, role_name, firstResult, maxResults);
	}

	public static List<UserRepresentation> getContextUsersByRole(String clientId, String role_name,
			Integer firstResult,
			Integer maxResults) {
		ClientResource clientResource = KkClientFactory.getSingleton().getKKClientById(clientId);
		RolesResource roles_resource = clientResource.roles();
		RoleResource r = roles_resource.get(role_name);

		List<UserRepresentation> users = r.getUserMembers(firstResult, maxResults);
		return users;
	}

	/**
	 * returns the list of users of the client
	 * users list is a subset of members list, it's obtained from the group named as
	 * the context
	 * 
	 * @param clientId null for current context
	 * @throws ServerException
	 * @throws NotFoundException
	 */

	public static List<UserRepresentation> getMemberGroupUsersCurrent() {
		return getMemberGroupUsers(null, null, null);
	}

	/**
	 * returns the list of users of the client
	 * users list is a subset of members list, it's obtained from the group named as
	 * the context
	 * 
	 * @param clientId    null for current context
	 * @param firstResult
	 * @param maxResults
	 * @throws ServerException
	 * @throws NotFoundException
	 */
	public static List<UserRepresentation> getMemberGroupUsers(String clientId, Integer firstResult,
			Integer maxResults) {
		RealmResource realmResource = KkClientFactory.getSingleton().getKKRealm();
		ClientResource clientResource = KkClientFactory.getSingleton().getKKClientById(clientId);
		ClientRepresentation client = clientResource.toRepresentation();

		GroupRepresentation g_repr = realmResource.getGroupByPath(client.getName());
		GroupResource group = realmResource.groups().group(g_repr.getId());

		List<UserRepresentation> user_members = group.members(firstResult, maxResults);
		return user_members;
	}
}
