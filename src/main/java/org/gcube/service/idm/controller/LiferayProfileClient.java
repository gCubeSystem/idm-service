package org.gcube.service.idm.controller;

import java.rmi.ServerException;

import org.gcube.service.idm.liferay.LiferayClientFactory;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.exception.UserRetrievalFault;
import org.gcube.vomanagement.usermanagement.impl.ws.LiferayWSUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.ServiceUnavailableException;

public class LiferayProfileClient {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LiferayProfileClient.class);

    public static GCubeUser getUserProfile(long id) {

        String errormsg = "cannot retrieve user with id " + id;
        try {
            LiferayWSUserManager client = LiferayClientFactory.getSingleton().getClient();

            GCubeUser user = client.getUserById(id);
            if (user == null) {
                throw new NotFoundException(errormsg);
            }
            return user;

        } catch (UserManagementSystemException e) {
            e.printStackTrace();
            throw new ServiceUnavailableException(errormsg);
        } catch (UserRetrievalFault e) {
            logger.warn(errormsg);
            e.printStackTrace();
            throw new NotFoundException(errormsg);
        }
    }

    public static GCubeUser getUserProfileByUsername(String username) {

        String errormsg = "cannot retrieve user with username " + username;
        try {
            LiferayWSUserManager client = LiferayClientFactory.getSingleton().getClient();

            GCubeUser user = client.getUserByUsername(username);
            if (user == null) {
                throw new NotFoundException(errormsg);
            }
            return user;

        } catch (UserManagementSystemException e) {
            logger.error(errormsg);

            e.printStackTrace();
            throw new ServiceUnavailableException(errormsg);
        } catch (UserRetrievalFault e) {
            logger.error(errormsg);
            e.printStackTrace();
            throw new NotFoundException(errormsg);
        } catch (Exception e) {
            logger.error(errormsg);
            e.printStackTrace();
            throw new ServiceUnavailableException(errormsg);
        }
    }

    public static GCubeUser getUserProfileByEmail(String email) {

        String errormsg = "cannot retrieve user with email " + email;
        try {
            LiferayWSUserManager client = LiferayClientFactory.getSingleton().getClient();

            GCubeUser user = client.getUserByEmail(email);
            if (user == null) {
                throw new NotFoundException(errormsg);
            }
            return user;

        } catch (UserManagementSystemException e) {
            e.printStackTrace();
            throw new ServiceUnavailableException(errormsg);
        } catch (UserRetrievalFault e) {
            logger.warn(errormsg);
            e.printStackTrace();
            throw new NotFoundException(errormsg);

        } catch (Exception e) {
            logger.warn(errormsg);
            e.printStackTrace();
            throw new ServiceUnavailableException(errormsg);
        }
    }

}
