package org.gcube.service.idm.controller;

import java.util.HashMap;
import java.util.Map;

import org.gcube.service.idm.serializers.IdmObjectSerializator;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class JWTController {

    public static Map<String, Object> decodeJwtToken(String token)
            throws JsonMappingException, JsonProcessingException {
        DecodedJWT decodedJWT = JWT.decode(token);

        String headerJson = IdmObjectSerializator.decodeBase64String(decodedJWT.getHeader());
        String payloadJson = IdmObjectSerializator.decodeBase64String(decodedJWT.getPayload());
        // String signatureJson =
        // ContextSerializator.decodeBase64String(decodedJWT.getSignature());

        Map<String, Object> decoded = new HashMap<String, Object>();
        decoded.put("jwt_token", token);
        decoded.put("token", decodedJWT.getToken());
        decoded.put("header", IdmObjectSerializator.jsonStringToHasmap(headerJson));
        decoded.put("payload", IdmObjectSerializator.jsonStringToHasmap(payloadJson));
        // decoded.put("signature",
        // ContextSerializator.jsonStringToHasmap(signatureJson));
        decoded.put("decodedJWT", decodedJWT);
        return decoded;
    }

}
