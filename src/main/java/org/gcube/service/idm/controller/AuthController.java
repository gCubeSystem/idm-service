package org.gcube.service.idm.controller;

import java.util.List;
import java.util.Map;

import javax.ws.rs.ForbiddenException;

import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;

public class AuthController {
    public final static String IDM_SERVICE_READ = "idm-service-read";

    // can admin current context
    public final static String IDM_SERVICE_ADMIN = "idm-service-admin";

    // can admin all realm, not only current context
    public final static String IDM_SERVICE_REALM = "idm-service-realm";

    public final static List<String> ACCESS_READ_ROLES = List.of(IDM_SERVICE_READ, IDM_SERVICE_ADMIN,
            IDM_SERVICE_REALM);
    public final static List<String> ACCESS_ADMIN_ROLES = List.of(IDM_SERVICE_ADMIN, IDM_SERVICE_REALM);
    public final static List<String> ACCESS_ADMIN_REALM_ROLES = List.of(IDM_SERVICE_REALM);

    public static String getAccessToken() {
        Map<String, String> authorizations = SecretManagerProvider.get().getHTTPAuthorizationHeaders();
        String access_token = authorizations.get("Authorization").replace("Bearer", "").trim();
        return access_token;
    }

    public static Owner getOwner() {
        Secret secret = SecretManagerProvider.get();
        Owner owner = secret.getOwner();
        return owner;
    }

    public static boolean checkRealmRole(String realm_role) {
        String access_token = getAccessToken();
        return checkRealmRole(realm_role, access_token);
    }

    public static boolean checkRealmRole(String realm_role, String access_token) {
        try {
            return ModelUtils.getAccessTokenFrom(access_token).getRealmAccess().getRoles().contains(realm_role);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkContextRole(String context_role) {
        Owner owner = getOwner();
        return checkContextRole(context_role, owner);
    }

    public static boolean checkContextRole(String context_role, Owner owner) {
        return owner.getRoles().contains(context_role);
    }

    public static boolean checkRole(String role) {
        return checkContextRole(role) || checkRealmRole(role);
    }

    public static boolean checkAnyRole(List<String> roles) {
        String access_token = getAccessToken();
        Owner owner = getOwner();

        for (String role : roles) {
            if (checkContextRole(role, owner) || checkRealmRole(role, access_token)) {
                return true;
            }
        }
        return false;
    }

    public static boolean userIsMe(String username) {
        Owner owner = getOwner();
        return userIsMe(username, owner);
    }

    public static boolean userIsMe(String username, Owner owner) {
        return !owner.isApplication() && owner.getId().equals(username);
    }

    public static void checkIsRealmAdmin(String message) throws ForbiddenException {
        if (!checkAnyRole(ACCESS_ADMIN_ROLES)) {
            throw new ForbiddenException(message);
        }
    }

    public static void checkIsContextmAdmin(String message) throws ForbiddenException {
        if (!checkAnyRole(ACCESS_ADMIN_REALM_ROLES)) {
            throw new ForbiddenException(message);
        }
    }

}
