package org.gcube.service.idm.controller;

import java.rmi.ServerException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.gcube.service.idm.keycloack.KkClientFactory;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.NotFoundException;

public class KCRolesController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(KCRolesController.class);

	public enum REPR {
		full, compact, name, id, none
	}

	public static Object formatRepr(RoleRepresentation role, REPR format) {
		if (role == null || format.equals(REPR.none)) {
			return null;
		}
		if (format.equals(REPR.name)) {
			return role.getName();
		}

		if (format.equals(REPR.id)) {
			return role.getId();
		}

		return role;
	}

	public static List<Object> formatList(List<RoleRepresentation> roles, REPR format) {
		return roles.stream().map(x -> formatRepr(x, format)).filter(Objects::nonNull).collect(Collectors.toList());

	}

	public static List<Object> getFormattedRoles(REPR format) {
		Boolean briefRepresentation = !format.equals(REPR.full);
		List<RoleRepresentation> roles = getRolesForContext(briefRepresentation);
		return formatList(roles, format);
	}

	public static List<RoleRepresentation> getRolesForContext() {
		return getRolesForContext(true);
	}

	public static List<RoleRepresentation> getRolesForContext(Boolean compact) {

		logger.info("Searching users for context");
		ClientResource client = KkClientFactory.getSingleton().getKKClient();
		RolesResource roles_resource = client.roles();

		List<RoleRepresentation> roles = roles_resource.list(compact);
		return roles;
	}

	public static RoleRepresentation getRoleByNameCurrent(String name) {
		logger.info("Searching users for context");
		ClientResource client = KkClientFactory.getSingleton().getKKClient();
		RolesResource roles_resource = client.roles();
		RoleResource role = roles_resource.get(name);
		if (role == null) {
			throw new NotFoundException("cannot retrieve role " + name);
		}

		return role.toRepresentation();
	}

	public static List<UserRepresentation> getUsersByRoleForContext(String role_name) {
		return getUsersByRoleForContext(role_name, null, null);

	}

	public static List<UserRepresentation> getUsersByRoleForContext(String role_name, Integer firstResult,
			Integer maxResults) {

		ClientResource client = KkClientFactory.getSingleton().getKKClient();
		RolesResource roles_resource = client.roles();
		RoleResource role_resource = roles_resource.get(role_name);

		List<UserRepresentation> users = role_resource.getUserMembers(firstResult, maxResults);
		return users;
	}

}
