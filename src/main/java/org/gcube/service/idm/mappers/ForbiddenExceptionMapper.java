package org.gcube.service.idm.mappers;

import org.gcube.service.utils.beans.ResponseBean;

import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * @author Alfredo Oliviero (ISTI - CNR)
 */
@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {

	@Override
	public Response toResponse(ForbiddenException exception) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		String exceptionMessage = exception.getMessage();

		ResponseBean responseBean = null;

		try {
			if (exception.getCause() != null) {
				exceptionMessage = exception.getCause().getMessage();
			}
		} catch (Exception e) {
			exceptionMessage = exception.getMessage();
		}

		MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;

		if (WebApplicationException.class.isAssignableFrom(exception.getClass())) {
			Response gotResponse = ((WebApplicationException) exception).getResponse();
			Object entity = gotResponse.getEntity();
			if (entity != null && ResponseBean.class.isAssignableFrom(entity.getClass())) {
				responseBean = (ResponseBean) entity;
			}
			status = Status.fromStatusCode(gotResponse.getStatusInfo().getStatusCode());
		}

		if (responseBean == null) {
			responseBean = new ResponseBean();
		}
		responseBean.setSuccess(false);
		responseBean.setMessage(exceptionMessage);
		// responseBean.set

		return Response.status(status).entity(responseBean).type(mediaType).build();
	}

}
