package org.gcube.service.idm.liferay;

import org.gcube.common.security.secrets.Secret;
import org.gcube.idm.common.is.InfrastrctureServiceClient;
import org.gcube.idm.common.is.IsServerConfig;
import org.gcube.vomanagement.usermanagement.impl.ws.LiferayWSUserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.InternalServerErrorException;

public class LiferayClientFactory {
	private static final Logger logger = LoggerFactory.getLogger(LoggerFactory.class);

	// Service endpoint properties
	private final static String RUNTIME_RESOURCE_NAME = "D4Science Infrastructure Gateway";
	private final static String CATEGORY = "Portal";
	private final static String END_POINT_NAME = "JSONWSUser";
	private final static boolean IS_ROOT_SERVICE = true;

	LiferayWSUserManager client = null;

	private IsServerConfig config;
	private Secret secret;

	private static LiferayClientFactory singleton = new LiferayClientFactory();

	public static LiferayClientFactory getSingleton() {
		if (singleton == null)
			singleton = new LiferayClientFactory();
		return singleton;
	}

	public Secret getSecret() {
		return secret;
	}

	public void setSecret(Secret secret) {
		this.secret = secret;
		this.config = fetchIsConfig(this.secret);
	}

	public IsServerConfig fetchIsConfig(Secret secret) throws InternalServerErrorException {
		try {
			if (this.secret == null)
				this.secret = InfrastrctureServiceClient.getSecretForInfrastructure();

			IsServerConfig cfg = InfrastrctureServiceClient.serviceConfigFromIS(RUNTIME_RESOURCE_NAME, CATEGORY,
					END_POINT_NAME, IS_ROOT_SERVICE, secret);
			logger.info("KeycloakAPICredentials object built {} - {}", cfg.getServerUrl(), cfg.getName());

			return cfg;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException(e);
		}
	}

	public LiferayWSUserManager createtLiferayClientInstance() {
		if (this.config == null) {
			this.config = fetchIsConfig(this.secret);
		}
		return createtLiferayClientInstance(this.config);
	}

	public static LiferayWSUserManager createtLiferayClientInstance(IsServerConfig config) {
		String host = config.getServerUrl();
		String schema = config.getProperty("schema");
		String user = config.getProperty("username");
		String password = config.getProperty("password");
		Integer port = Integer.valueOf(config.getProperty("port"));

		LiferayWSUserManager client = null;
		try {
			client = new LiferayWSUserManager(user, password, host, schema, port);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InternalServerErrorException("cannot create Liferay client");
		}

		if (client == null) {
			throw new InternalServerErrorException("cannot create Liferay client");
		}

		logger.info("Liferay object built {} - {}", config.getServerUrl(), config.getName());

		return client;
	}

	// public IsServerConfig getConfig() {
	// if (this.config == null) {
	// this.config = fetchIsConfig();
	// }
	// return this.config;
	// }

	public LiferayWSUserManager getClient() {
		if (this.client == null) {
			this.client = createtLiferayClientInstance();
		}
		return this.client;
	}

}
