/**
 * <h1>Identity Manager (IDM) Service</h1>
 * 
 * <p>
 * Welcome to Identity Manager Service (aka IDM) API documentation.
 * </p>
 * 
 * <p>
 * To get a complete overview of gCat service take a look at
 * <a href="../docs/index.html">wiki page</a>.
 * </p>
 * 
 * 
 */
package org.gcube.service.idm.rest;
