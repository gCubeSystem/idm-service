package org.gcube.service.idm.rest;

import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.service.idm.IdMManager;
import org.gcube.service.idm.controller.LiferayProfileClient;
import org.gcube.service.utils.ErrorMessages;
import org.gcube.service.utils.beans.ResponseBeanMap;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * @responseExample application/json {
 *                  "success": true,
 *                  "message": null,
 *                  "result": {
 *                  "roles": [
 *                  "Catalogue-Editor",
 *                  "Member"
 *                  ],
 *                  "context": "[CONTEXT]",
 *                  "avatar":
 *                  "https://www.d4science.org:443/image/user_male_portrait?img_id=0&img_id_token=AAABBBCCC",
 *                  "fullname": "FirstName LastName",
 *                  "username": "user.name"
 *                  }
 *                  }
 */
@ManagedBy(IdMManager.class)
@RequestHeaders({
        @RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
        @RequestHeader(name = "Content-Type", description = "application/json")
})
@Path("2/people")
// @ResourceGroup("Users APIs")
// @ResourceLabel("Greetings APIs")
// @RequestHeaders({
// @RequestHeader(name = "Authorization", description = "Bearer token, see <a
// href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
// })
public class SocialPeopleAPI {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SocialPeopleAPI.class);

    /**
     * @responseExample application/json { "success" : true, "message" : null,
     *                  "result" : { "roles" : [ ], "context" : "***", "avatar" :
     *                  "https://*****3D", "fullname" : "John Smith", "username" :
     *                  "john.smith" } }
     * @return the user's profile. The user in this case is the one bound to the
     *         token
     */
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("profile")
    @StatusCodes({
            @ResponseCode(code = 200, condition = "Successful retrieval of user's profile, reported in the 'result' field of the returned object"),
            @ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
    })
    public Response getProfile() {

        ResponseBeanMap responseBean = new ResponseBeanMap();
        Status status = Status.OK;

        Secret secret = SecretManagerProvider.get();
        Owner owner = secret.getOwner();
        String username = owner.getId();

        String context = secret.getContext();
        if (owner.isApplication()) {
            logger.warn("Trying to access users method via a token different than USER is not allowed");

            // only users can use "me"
            throw new ForbiddenException(ErrorMessages.NOT_USER_TOKEN_CONTEXT_USED);
        }

        try {
            GCubeUser profile = LiferayProfileClient.getUserProfileByUsername(username);

            responseBean.putResult("username", username);
            responseBean.putResult("avatar", profile.getUserAvatarURL());
            responseBean.putResult("fullname", owner.getFirstName() + " " + owner.getLastName());
            responseBean.putResult("context", context);
            responseBean.putResult("roles", owner.getRoles());

            // UserManager userManager =
            // UserManagerWSBuilder.getInstance().getUserManager();
            // RoleManager roleManager =
            // RoleManagerWSBuilder.getInstance().getRoleManager();
            // GroupManager groupManager =
            // GroupManagerWSBuilder.getInstance().getGroupManager();
            // user = userManager.getUserByUsername(username);

            // List<GCubeRole> roles = roleManager.listRolesByUserAndGroup(user.getUserId(),
            // groupManager.getGroupIdFromInfrastructureScope(scope));
            // List<String> rolesNames = new ArrayList<String>();
            // for (GCubeRole gCubeRole : roles) {
            // rolesNames.add(gCubeRole.getRoleName());
            // }
            // toReturn.put("roles", rolesNames);

            responseBean.setSuccess(true);
        } catch (Exception e) {
            logger.error("Unable to retrieve user's profile", e);
            responseBean.setMessage(e.getMessage());
            status = Status.INTERNAL_SERVER_ERROR;
        }

        return Response.status(status).entity(responseBean).build();

    }
}
