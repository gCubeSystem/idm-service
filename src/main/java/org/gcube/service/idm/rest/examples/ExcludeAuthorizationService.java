package org.gcube.service.idm.rest.examples;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("guest")
public class ExcludeAuthorizationService {

	private final Logger logger = LoggerFactory.getLogger(ExcludeAuthorizationService.class);

	/**
	 * this method doesn't need authorization and the SecretManagerProvider is null
	 * see to implement this behavior add to excludes section in your
	 * application.yaml
	 * 
	 * - path: /{path-to-your-method-path}
	 * 
	 * example for this method
	 * 
	 * - path: /excluded
	 * 
	 */
	@GET
	public String exludedMethod() {
		logger.info("executed whithout any authorization");
		return "executed whithout any authorization";
	}
}
