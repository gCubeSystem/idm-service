package org.gcube.service.idm.rest;

import java.util.List;
import java.util.Set;

import org.gcube.service.idm.IdMManager;
import org.gcube.service.idm.controller.KCRolesController;
import org.gcube.service.idm.controller.KCUserController;
import org.gcube.service.idm.keycloack.KkClientFactory;
import org.gcube.service.idm.serializers.IdmObjectSerializator;
import org.gcube.service.utils.beans.ResponseBean;
import org.gcube.service.utils.beans.ResponseBeanMap;
import org.gcube.service.utils.beans.ResponseBeanPaginated;
import org.gcube.smartgears.annotations.ManagedBy;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@ManagedBy(IdMManager.class)
@RequestHeaders({
        @RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
        @RequestHeader(name = "Content-Type", description = "application/json")
})
@Path("roles")
public class RolesAPI {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RolesAPI.class);

    /**
     * Returns roles in context
     * 
     * @param format      roles response format
     * @param firstResult pagination offset
     * @param maxResults  maximum results size
     * @param search      filter by name
     */
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response search(@QueryParam("search") @DefaultValue("") String search,
            @QueryParam("first") @DefaultValue("0") int firstResult,
            @QueryParam("max") @DefaultValue("100") int maxResults,
            @QueryParam("format") @DefaultValue("name") KCRolesController.REPR format) {
        ResponseBean responseBean = new ResponseBeanPaginated(firstResult, maxResults);

        Boolean briefRepresentation = !KCRolesController.REPR.full.equals(format);

        try {
            ClientResource client = KkClientFactory.getSingleton().getKKClient();
            List<RoleRepresentation> roles = client.roles().list(search, firstResult, maxResults, briefRepresentation);

            responseBean.setResult(KCRolesController.formatList(roles, format));
            responseBean.setSuccess(true);

            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }

    /**
     * Returns role by name
     * 
     * @path role_name the role
     *       'email' and 'username' must match exactly. default true
     */
    @GET
    @Path("/{role_name}")
    @Produces({ "application/json;charset=UTF-8", "application/vnd.api+json" })
    public Response role(
            @PathParam("role_name") String role_name) {
        ResponseBean responseBean = new ResponseBean();

        try {
            RoleRepresentation role = KCRolesController.getRoleByNameCurrent(role_name);

            responseBean.setResult(role);
            responseBean.setSuccess(true);

            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }

    /**
     * Returns the list of members (users + services_accounts) with the given role
     * in the context
     * 
     * @param format      users response format
     * @param role_name   the role
     * @param firstResult pagination offset
     * @param maxResults  maximum results size
     */
    @GET
    @Path("/{role_name}/members")
    @Produces({ "application/json;charset=UTF-8", "application/vnd.api+json" })
    public Response membersForRole(
            @PathParam("role_name") String role_name,
            @QueryParam("first") @DefaultValue("0") int firstResult,
            @QueryParam("max") @DefaultValue("100") int maxResults,
            @QueryParam("format") @DefaultValue("username") KCUserController.REPR format) {
        ResponseBean responseBean = new ResponseBeanPaginated(firstResult, maxResults);

        try {
            ClientResource client = KkClientFactory.getSingleton().getKKClient();
            RolesResource roles_resource = client.roles();
            RoleResource r = roles_resource.get(role_name);

            List<UserRepresentation> users = r.getUserMembers(firstResult, maxResults);

            responseBean.setResult(KCUserController.formatList(users, format));
            responseBean.setSuccess(true);

            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }

    /**
     * Returns the list of users with role in the context
     * 
     * @param format      users response format
     * @param role_name   the role
     * @param firstResult pagination offset
     * @param maxResults  maximum results size
     */
    @GET
    @Path("/{role_name}/users")
    @Produces({ "application/json;charset=UTF-8", "application/vnd.api+json" })
    public Response usersForRole(
            @PathParam("role_name") String role_name,
            @QueryParam("first") @DefaultValue("0") int firstResult,
            @QueryParam("max") @DefaultValue("100") int maxResults,
            @QueryParam("format") @DefaultValue("username") KCUserController.REPR format) {
        ResponseBeanMap responseBean = new ResponseBeanMap();
        try {
            ClientResource client = KkClientFactory.getSingleton().getKKClient();
            RolesResource roles_resource = client.roles();
            RoleResource r = roles_resource.get(role_name);

            // ruoli che danno quel
            Set<GroupRepresentation> groups = r.getRoleGroupMembers(firstResult, maxResults);
            responseBean.putResult("roleGroupMembers", groups);

            List<UserRepresentation> users = r.getUserMembers();
            responseBean.putResult("users", users);

            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
        // throw new InternalServerErrorException("not implemented");
    }

    /**
     * Returns the list of users with role in the context
     * 
     * @param format      users response format
     * @param role_name   the role
     * @param firstResult pagination offset
     * @param maxResults  maximum results size
     */
    @GET
    @Path("/{role_name}/service_accounts")
    @Produces({ "application/json;charset=UTF-8", "application/vnd.api+json" })
    public Response servicesAccountsForRole(
            @PathParam("role_name") String role_name,
            @QueryParam("first") @DefaultValue("0") int firstResult,
            @QueryParam("max") @DefaultValue("100") int maxResults,
            @QueryParam("format") @DefaultValue("username") KCUserController.REPR format) {
        throw new InternalServerErrorException("not implemented");
    }
}
