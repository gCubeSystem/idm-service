package org.gcube.service.idm.rest;

import java.util.HashMap;
import java.util.Map;

import org.gcube.service.idm.controller.AuthController;
import org.gcube.service.idm.controller.JWTController;
import org.gcube.service.idm.serializers.IdmObjectSerializator;
import org.gcube.service.utils.ErrorMessages;
import org.gcube.service.utils.beans.ResponseBean;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@RequestHeaders({
        @RequestHeader(name = "Authorization", description = "Bearer token, see https://dev.d4science.org/how-to-access-resources"),
        @RequestHeader(name = "Content-Type", description = "application/json")
})
@Path("jwt")
public class JwtAPI {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JwtAPI.class);

    @GET
    @Path("/decode")
    @Produces(MediaType.APPLICATION_JSON)
    @StatusCodes({
            @ResponseCode(code = 200, condition = "decode the token"),
            @ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
    })

    public Response getDecodedJwtToken(
            @QueryParam("token") String token) {
        // Status status = Status.OK;

        ResponseBean responseBean = new ResponseBean();

        try {
            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();
            Map<String, Object> decoded = JWTController.decodeJwtToken(token);
            responseBean.setResult(decoded);
            responseBean.setSuccess(true);

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }

    @GET
    @Path("/auth")
    @Produces({ "application/json;charset=UTF-8", "application/vnd.api+json" })
    @StatusCodes({
            @ResponseCode(code = 200, condition = "The user's email is reported in the 'result' field of the returned object"),
            @ResponseCode(code = 500, condition = ErrorMessages.ERROR_IN_API_RESULT)
    })
    public Response getDecodedJwtAuth() {
        Status status = Status.OK;

        ResponseBean responseBean = new ResponseBean();

        String token = AuthController.getAccessToken();
        try {
            ObjectMapper objectMapper = IdmObjectSerializator.getSerializer();

            Map<String, Object> response = new HashMap<String, Object>();

            response.put("auth_token", JWTController.decodeJwtToken(token));
            // response.put("authorizations", authorizations);

            responseBean.setResult(response);
            responseBean.setSuccess(true);

            String jsonData = objectMapper.writeValueAsString(responseBean);
            return Response.ok(jsonData).build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }
}
