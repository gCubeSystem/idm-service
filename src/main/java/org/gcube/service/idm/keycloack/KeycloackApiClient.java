package org.gcube.service.idm.keycloack;

import org.keycloak.admin.client.Keycloak;

public class KeycloackApiClient {
    public Keycloak kclient;
    public String realmName;
    public String clientIdContext;
    public String context;

    public static String encodeClientIdContext(String context) {
        return context.replace("/", "%2F");
    }

    public KeycloackApiClient(Keycloak kclient, String realmName, String context) {
        this.clientIdContext = encodeClientIdContext(context);
        this.context = context;
        this.kclient = kclient;
        this.realmName = realmName;
        // ClientsResource clients = kclient.realm(realmName).clients().get*
        // clients.get(context);
    }
}
