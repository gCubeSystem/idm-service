package org.gcube.service.idm.serializers;

import java.io.IOException;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson serializer for ServiceEndpoint.AccessPoint
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class ServiceEndpointAccessPointSerializer extends StdSerializer<ServiceEndpoint.AccessPoint> {

    protected ServiceEndpointAccessPointSerializer(Class<ServiceEndpoint.AccessPoint> t) {
        super(t);
    }

    public ServiceEndpointAccessPointSerializer() {
        super(ServiceEndpoint.AccessPoint.class, true);
    }

    @Override
    public void serialize(ServiceEndpoint.AccessPoint accessPoint, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();

        String error = null;
        String clientSecret = null;
        try {
            clientSecret = StringEncrypter.getEncrypter().decrypt(accessPoint.password());
        } catch (Exception e) {
            error = e.getMessage();
        }

        jgen.writeStringField("address", accessPoint.address());
        jgen.writeStringField("name", accessPoint.name());
        jgen.writeStringField("description", accessPoint.description());

        try {
            jgen.writeStringField("username", accessPoint.username());
        } catch (Exception e) {
            jgen.writeStringField("username", null);
        }
        try {
            jgen.writeStringField("tostring", accessPoint.toString());
        } catch (Exception e) {
            jgen.writeStringField("tostring", null);
        }
        if (error != null) {
            jgen.writeStringField("key_error", error);
            jgen.writeBooleanField("key_decoded", false);
        } else {
            jgen.writeBooleanField("key_decoded", true);
        }
        jgen.writeEndObject();
    }
}
