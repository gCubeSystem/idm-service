package org.gcube.service.idm.serializers;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;

import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.security.SimpleCredentials;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Jackson Serialization utils for Smartgear Context classes
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class IdmObjectSerializator {
    private static ObjectMapper serializer = null;

    protected static TypeReference<HashMap<String, Object>> typeRefHashmap = new TypeReference<HashMap<String, Object>>() {
    };

    public static String decodeBase64String(String encodedString) {
        return new String(Base64.getUrlDecoder().decode(encodedString), StandardCharsets.UTF_8);
    }

    public static HashMap<String, Object> jsonStringToHasmap(String jsonString)
            throws JsonMappingException, JsonProcessingException {
        return getSerializer().readValue(jsonString, typeRefHashmap);
    }

    public static ObjectMapper getSerializer() {
        if (serializer == null) {
            ObjectMapper om = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            // module.addSerializer(Owner.class, new OwnerSerializer());

            module.addSerializer(ContainerConfiguration.class, new ContainerConfigurationSerializer());
            module.addSerializer(ContainerContext.class, new ContainerContextSerializer());
            module.addSerializer(SimpleCredentials.class, new SimpleCredentialsSerializer());

            // DecodedJWT serialization
            om.registerModule(new JavaTimeModule());
            serializer = om;
        }
        return serializer;
    }

}
