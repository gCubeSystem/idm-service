package org.gcube.service.utils.beans;

/**
 * Response bean
 *
 */
public class ResponseBeanPaginated extends ResponseBean {

    private static final long serialVersionUID = -2725238162673879658L;

    protected Integer firstResult;

    protected Integer maxResults;

    public Integer getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public ResponseBeanPaginated() {
        super();
    }

    // /**
    // * @param success
    // * @param message
    // * @param result
    // */
    // public ResponseBeanPaginated(boolean success, String message, Object result)
    // {
    // super(success, message, result);
    // }

    /**
     * @param firstResult
     * @param maxResults
     */
    public ResponseBeanPaginated(Integer firstResult, Integer maxResults) {
        this.firstResult = firstResult;
        this.maxResults = maxResults;
    }

    /**
     * @param success
     * @param message
     * @param result
     * @param firstResult
     * @param maxResults
     */
    public ResponseBeanPaginated(boolean success, String message, Object result, Integer firstResult,
            Integer maxResults) {
        super(success, message, result);
        this.firstResult = firstResult;
        this.maxResults = maxResults;
    }

    @Override
    public String toString() {
        return "ResponseBean [success=" + success
                + ", message=" + message + ", result=" + result + ", firstResult=" + firstResult + ", maxResults="
                + maxResults + "]";
    }
}
