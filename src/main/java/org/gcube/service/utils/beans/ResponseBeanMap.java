package org.gcube.service.utils.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Response bean
 *
 */
public class ResponseBeanMap extends ResponseBean {

    private static final long serialVersionUID = -2725238162673879658L;
    /**
     * The result of the request: true if it succeeded, false otherwise
     */
    protected boolean success;

    /**
     * An error message if something wrong happened, null/empty otherwise
     */
    protected String message;
    /**
     * The result object of the request
     */
    protected Map<String, Object> result = new HashMap<String, Object>();

    public ResponseBeanMap() {
        super();
    }

    /**
     * @param success
     * @param message
     * @param result
     */
    public ResponseBeanMap(boolean success, String message, Map<String, Object> mapResults) {
        super();
        this.success = success;
        this.message = message;
        this.result = mapResults;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> mapResults) {
        this.result = mapResults;
    }

    public void putResult(String key, Object res) {
        this.result.put(key, res);
    }

    @Override
    public String toString() {
        return "ResponseBean [success=" + success
                + ", message=" + message + ", result=" + result + "]";
    }
}
