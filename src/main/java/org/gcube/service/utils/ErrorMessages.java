package org.gcube.service.utils;

public class ErrorMessages {

	public static final String ERROR_IN_API_RESULT = "The error is reported into the 'message' field of the returned object";
	public static final String INVALID_ATTRIBUTE = "Such an attribute doesn't exist";
	public static final String NOT_USER_TOKEN_CONTEXT_USED = "User's information can only be retrieved through a user token (not qualified)";
	public static final String NOT_SERVICE_TOKEN_CONTEXT_USED = "This method can only be called with an infrastructure token";
	public static final String USER_NOT_AUTHORIZED_PRIVATE = "User is not authorized to access private data";
	public static final String CANNOT_RETRIEVE_PROFILE = "Unable to retrieve user profile";

	public static final String RESERVED_PARAMETER = "The parameter can be used only by realm administrators: ";

	//
	// protected static final String CANNOT_RETRIEVE_SERVICE_ENDPOINT_INFORMATION =
	// "Unable to retrieve such service endpoint information";

	// private static final String NO_RUNTIME_RESOURCE_TEMPLATE_NAME_CATEGORY =
	// "There is no Runtime Resource having name %s and Category %s in this scope";

	// public static final String MISSING_TOKEN = "Missing token.";
	// public static final String MISSING_PARAMETERS = "Missing request
	// parameters.";
	// public static final String INVALID_TOKEN = "Invalid token.";
	// public static final String TOKEN_GENERATION_APP_FAILED = "Token generation
	// failed.";
	// public static final String NOT_APP_TOKEN = "Invalid token: not belonging to
	// an application.";
	// public static final String NOT_APP_ID = "Invalid application id: it doesn't
	// belong to an application.";
	// public static final String NO_APP_PROFILE_FOUND = "There is no application
	// profile for this app id/scope.";
	// public static final String BAD_REQUEST = "Please check the parameter you
	// passed, it seems a bad request";
	// public static final String POST_OUTSIDE_VRE = "A post cannot be written into
	// a context that is not a VRE";
	// public static final String DEPRECATED_METHOD = "This method is deprecated,
	// must use version 2";

}
