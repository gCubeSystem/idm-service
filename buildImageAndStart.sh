#!/bin/bash

# set -x   # uncomment to debug script

ACCEPTED_JAVA_VERSIONs=(11 17)

NAME=idm-service
BUILD_VERSION=0.0.1-SNAPSHOT
SMARTGEARS_VERSION=4.0.1-SNAPSHOT
CONTAINER_INI="./docker/container.ini"
IMAGE_VERSION=${BUILD_VERSION}-java${JAVA_VERSION}-smartgears${SMARTGEARS_VERSION}

PORT=8080
DEBUG_PORT=5005
DEBUG=false
EXECUTE=false
TEST=false
COMPILE=true
JAVA_VERSION=11

PUSH_DOCKER=false
PUSH_HARBOR=false
LOGIN_HARBOR=false

BUILD_NAME=$NAME:$IMAGE_VERSION

echo "BUILD_NAME=$BUILD_NAME"

################################################################################
# Help                                                                         #
################################################################################
Help() {
    # Display Help
    echo "build, create and run in docker the identity manager service"
    echo
    echo "Syntax: buildDistribution [-n arg] [-p arg] [-j arg] [-d arg?] [-h]"
    echo "options:"
    echo "-s        skip maven package"
    echo "-t        exec also maven tests"
    echo "-n arg    specifies the docker image name (default is identity-manager)."
    echo "-p arg    specifies the port to be exposed for the docker container to access the service (default $PORT)"
    echo "-j arg    specify java version (default is $JAVA_VERSION)"
    echo "          accepted version are: ${ACCEPTED_JAVA_VERSIONs[@]}"
    echo "-e        execute the image"
    echo "-d arg?   enable java debug mode for execution"
    echo "          arg is the debug port (default is $DEBUG_PORT)"
    echo "-r        push image to d4science harbo[r] (with login already done, or -l to login)"
    echo "-l        [l]ogin to d4science harbor"
    echo "-u        p[u]sh image to dockerhub (with docker login already done)"
    echo "-c arg      path of the file to deploy as container.ini (default ./docker/container.ini)"
    echo "-h        Print this Help."
    echo
}

################################################################################
################################################################################
# Main program                                                                 #
################################################################################
################################################################################

set -e

OPTSTRING=":slmurn:p:c:ed:j:?h"

while getopts $OPTSTRING opt; do
    # echo "Option -${opt} was triggered, Argument: ${OPTARG}"
    case "${opt}" in
    s) COMPILE=false && echo "compile $COMPILE" ;;
    c) 
        CONTAINER_INI=${OPTARG} 
        echo "CONTAINER_INI: $CONTAINER_INI";;
    m) MULTI_PLATFORM=true ;;
    n) NAME=${OPTARG} ;;
    p) PORT=${OPTARG} ;;

    u) PUSH_DOCKER=true ;;
    l) LOGIN_HARBOR=true ;;
    r) PUSH_HARBOR=true ;;

    t) TEST=true ;;
    e) EXECUTE=true ;;
    d)
        EXECUTE=true
        DEBUG=true
        DEBUG_PORT=${OPTARG}
        echo "debug enabled, port $DEBUG_PORT"
        ;;
    j)
        if [[ ${ACCEPTED_JAVA_VERSIONs[@]} =~ ${OPTARG} ]]; then
            JAVA_VERSION=${OPTARG}
        else
            echo "Invalid java version" && echo "accepted version are: ${ACCEPTED_JAVA_VERSIONs[@]}" && exit 1
        fi
        ;;
    h) Help && exit 0 ;;

    # matched when an option that is expected to have an argument is passed without one
    :)
        if [ ${OPTARG} = "d" ]; then
            DEBUG=true
            echo "debug enabled, port $DEBUG_PORT"
        else
            # matched when an option that is expected to have an argument is passed without one
            echo "Option -${OPTARG} requires an argument."
            exit 1
        fi
        ;;
    ?) # match any invalid option that is passed
        echo "Invalid option: -${OPTARG}."
        exit 1
        ;;
    esac
done

if [ $COMPILE = true ]; then
    if [ $TEST = false ]; then
        mvn clean package -Dmaven.test.skip
    else
        mvn clean package
    fi
else
    echo "skipping mvn package"
fi

if [ -z $MULTI_PLATFORM ]; then
    docker build -t $BUILD_NAME --build-arg="CONTAINER_INI=$CONTAINER_INI" --build-arg="JAVA_VERSION=${JAVA_VERSION}" --build-arg="SMARTGEARS_VERSION=${SMARTGEARS_VERSION}" .
else
    docker build -t $BUILD_NAME --build-arg="CONTAINER_INI=$CONTAINER_INI" --build-arg="JAVA_VERSION=${JAVA_VERSION}" --build-arg="SMARTGEARS_VERSION=${SMARTGEARS_VERSION}" --platform=linux/amd64,linux/arm64,linux/arm/v7 .
    
    # docker manifest create hub.dev.d4science.org/gcube/$BUILD_NAME \
    #     hub.dev.d4science.org/gcube/$NAME-amd64-linux:$IMAGE_VERSION \
    #     hub.dev.d4science.org/gcube/$NAME-arm-linux:$IMAGE_VERSION \
    #     hub.dev.d4science.org/gcube/$NAME-arm-linux:$IMAGE_VERSION 
fi

if [ ${PUSH_DOCKER} = true ]; then
    DOCKER_NAME=d4science/$BUILD_NAME
    docker tag $BUILD_NAME $DOCKER_NAME
    docker push $DOCKER_NAME
    echo ">>> pushed on dockerhub the image $DOCKER_NAME"
fi

if [ ${LOGIN_HARBOR} = true ]; then
    ./loginHarborHub.sh
fi

if [ $PUSH_HARBOR = true ]; then
    HARBOR_NAME=hub.dev.d4science.org/gcube/$BUILD_NAME
    echo ">>> PUSHING on hub.dev.d4science.org the image $HARBOR_NAME"

    docker tag $BUILD_NAME $HARBOR_NAME
    docker push $HARBOR_NAME
    echo ">>> pushed on hub.dev.d4science.org the image $HARBOR_NAME"
fi

if [ $EXECUTE = true ]; then
    if [ $DEBUG = true ]; then
        docker run -p $PORT:8080 -p $DEBUG_PORT:5005 -e JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=*:5005,server=y,suspend=y" $BUILD_NAME
    else
        docker run -p $PORT:8080 $BUILD_NAME
    fi
fi
