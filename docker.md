mvn clean package
# docker build -t idm .
docker compose up -d --build --force-recreate

# senza composer
#docker run -it -d -p 8080:8080  --name idm idm

# controllare servizio
curl http://localhost:8080/identity-manager/gcube/resource/health
