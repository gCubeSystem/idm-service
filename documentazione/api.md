<!-- // da implementare rispetto al contesto	 -->

* /2/users/get-profile // profilo utente corrente
* /2/users/get-email // utente corrente
* /2/users/get-fullname // utente corrente

* /2/users/get-all-usernames
* /2/users/get-all-fullnames-and-usernames
* /2/users/get-usernames-by-role
* /2/users/user-exists // https://howtodoinjava.com/devops/search-keycloak-users/

* // attenzione al risultato. vedere in seguito
* /2/users/get-oauth-profile  

* /2/users/get-custom-attribute
* /2/users/get-usernames-by-global-role

* /2/people/profile

# REF:
https://www.keycloak.org/docs-api/22.0.1/rest-api/index.html#_users
https://howtodoinjava.com/devops/search-keycloak-users/

# Altro

* implementare anche restified

* aggiungere api e credenziali x servizi
  * nel token trovo se la richesta arriva da un utente o un servizio
  * possono richiedere info e profili utente
  * bisogna esporre anche il loro profilo, ora è su ids non su keycloak
  * 
* 

rif. https://api.dev.d4science.org/social-networking-library-ws/api-docs/index.html

implementazione social: https://code-repo.d4science.org/gCubeSystem/social-networking-library-ws/src/branch/master/src/main/java/org/gcube/portal/social/networking/ws/methods/v2/Users.java
