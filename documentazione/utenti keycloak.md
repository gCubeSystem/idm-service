# Keycloack

## ambiente di sviluppo:
keycloack: https://next.dev.d4science.org/
liferay: https://next.dev.d4science.org/group/control_panel?refererPlid=41831713&p_p_id=134

site: 
* /gcube
* /gcube/devNext
* /gcube/devsec/devVRE (usare questa)

realm: usare d4science

gruppi: https://accounts.dev.d4science.org/auth/admin/master/console/#/d4science/groups

usiamo  /gcube/devsec/devVRE https://accounts.dev.d4science.org/auth/admin/master/console/#/d4science/groups/7097b82d-8bab-456b-a0c6-691bd303367b/32104d02-96b7-4726-839e-874089b52e2a/27840d29-a476-445a-b64a-21e25fa65be5


membri:
* liferay https://next.dev.d4science.org/group/control_panel/manage?p_p_auth=j7NeN5EX&p_p_id=174&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&doAsGroupId=21678&refererPlid=41831713&controlPanelCategory=sites

* keycloack https://accounts.dev.d4science.org/auth/admin/master/console/#/d4science/groups/7097b82d-8bab-456b-a0c6-691bd303367b/32104d02-96b7-4726-839e-874089b52e2a/27840d29-a476-445a-b64a-21e25fa65be5


contesti
* su su keycloack, clients  (usa %2f al posto di /)
* su liferay, sites liferay 

ruoli:
* Accounting-Manager	False	Accounting-Manager role	
* Catalogue-Admin	False	Catalogue-Admin role	
* Catalogue-Editor	False	Catalogue-Editor role	
* Catalogue-Manager	False	Catalogue-Manager role	
* Catalogue-Moderator	False	Catalogue-Moderator role	
* Data-Editor	False	—	
* Data-Manager	False	Data-Manager role	
* DataMiner-Manager	False	DataMiner-Manager role	
* Infrastructure-Manager	False	Infrastructure-Manager role	
* Member	False	Member role	
* VO-Admin	False	VO-Admin role	
* VRE-Designer	False	VRE-Designer role	
* VRE-Manager	False	VRE-Manager role	
1 - 15


postman api
clientid id.d4science.org => https://accounts.dev.d4science.org/auth/admin/master/console/#/d4science/clients/db98093f-3c91-4d6f-ab20-249b17cf25b2/settings

service client access:  
* url https://accounts.dev.d4science.org/auth
* client_id id.d4science.org
* client secret 09c26f24-3c65-4039-9fa0-e5cc4f4032cd
* grant type: client_credentials

09c26f24-3c65-4039-9fa0-e5cc4f4032cd


usare /auth/realm