# installare java

## aggiungere repo temurin
brew install --cask temurin
brew tap homebrew/cask-versions

# installare le versioni 
brew install --cask temurin11
brew install --cask temurin8
brew install --cask temurin21

# 

# https://stackoverflow.com/a/68105964/2473953

# lista JVM installate
# /usr/libexec/java_home -V 

# java home versions
export JAVA_8_HOME=$(/usr/libexec/java_home -v1.8)
export JAVA_11_HOME=$(/usr/libexec/java_home -v11)
export JAVA_17_HOME=$(/usr/libexec/java_home -v17)
#export JAVA_18_HOME=$(/usr/libexec/java_home -v18)
#export JAVA_19_HOME=$(/usr/libexec/java_home -v19)
#export JAVA_21_HOME=$(/usr/libexec/java_home -v21)

# alias to set defaults
alias java8='export JAVA_HOME=$JAVA_8_HOME'
alias java11='export JAVA_HOME=$JAVA_11_HOME'
alias java17='export JAVA_HOME=$JAVA_17_HOME'
#alias java18='export JAVA_HOME=$JAVA_18_HOME'
#alias java19='export JAVA_HOME=$JAVA_19_HOME'
#alias java21='export JAVA_HOME=$JAVA_21_HOME'

# set default to java11
java11
