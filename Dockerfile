ARG JAVA_VERSION=11
ARG SMARTGEARS_VERSION=4.0.0-SNAPSHOT

#FROM d4science/smartgears-distribution:${SMARTGEARS_VERSION}-java${JAVA_VERSION}-tomcat10.1.19
FROM hub.dev.d4science.org/gcube/smartgears-distribution:${SMARTGEARS_VERSION}-java${JAVA_VERSION}-tomcat10.1.19
    #FROM smartgears-distribution:4.0.0-SNAPSHOT-java$JAVA_VERSION-tomcat10.1.19
    ARG CONTAINER_INI="./docker/container.ini"

    COPY ./docker/logback.xml /etc/
    COPY ${CONTAINER_INI} /etc/container.ini
    COPY ./docker/*.gcubekey /tomcat/lib
    COPY ./target/idm-service.war /tomcat/webapps/

    EXPOSE 8080
