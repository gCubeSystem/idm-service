# OPENSTACK

* [ ] deploy su openstack
* [ ] aggiungere accounting

# CONFIGURAZIONE

* [ ] leggere parametri da file di container.ini
* [ ] rivedere gestione errori
  
# MASSI

* API REST social
  * [ ] stesso path (con 2/ davanti)
  * [ ] verificare di accettare esattamente stessi parametri
  * [ ]usare stesso bean (senza limit e max )

* nel token abbiamo i ruoli realm globali. al momento smartgear li ignora, bisogna implementare logica di accesso basata su quei ruoli. verificare se smartgear li legge, in caso contrario fare una chiamata di appoggio che li ottiene dal token, versione futura si appoggerà su smartgear
*  "realm_access": 
{
    "roles": [
        "offline_access",
        "uma_authorization",
        "service-endpoint-key"
    ]
},

* [ ] implementare client (rif. gcat-client)
* [ ] verificare controllo diritti

