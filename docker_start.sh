mkdir -p tomcat/logs
mkdir -p tomcat/webapps

rm -fr tomcat/logs/*
rm -fr tomcat/webapps/*

mvn clean package
docker compose up -d --build --force-recreate
# curl http://localhost:8080/identity-manager/gcube/resource/healthx
